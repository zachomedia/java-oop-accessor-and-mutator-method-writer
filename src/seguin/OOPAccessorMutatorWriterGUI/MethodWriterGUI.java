package seguin.OOPAccessorMutatorWriterGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.UIManager.*;
import java.io.*;

import seguin.OOPAccessorMutatorWriter.*;

/**
 * Create the Graphical User Interface (GUI) for the OOP Accessor and Mutator Method Writer application.
 * 
 * @author Zachary Seguin
 * @version 1.2, 23/05/2012
 * @since 1.0
 */
public class MethodWriterGUI extends JFrame implements ActionListener
{
    /**
     * The width of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_WIDTH = 375;
    
    /**
     * The height of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_HEIGHT = 295;
    
    /**
     * The width of the padding around the JFrame.
     *
     * @since 1.0
     */
    public static int PADDING_SIZE = 5;
    
    /**
     * The window title.
     *
     * @since 1.0
     */
    public static String WINDOW_TITLE = "OOP Accessors and Mutators Writer | Designed by Zachary Seguin";
    
    private static String METHODS_SELECTION_PANEL_TITLE = "Add";
    private static String ACCESSOR_METHODS_TITLE = "Accessor Methods";
    private static String MUTATOR_METHODS_TITLE = "Mutator Methods";
    
    private static String OPTIONS_PANEL_TITLE = "Settings";
    private static String COMMENTS_STYLE_TITLE = "Comment Style";
    
    private static String ADD_METHODS_BUTTON_TITLE = "Add Methods";
    private static String HOW_TO_USE_BUTTON_TITLE = "How to Use";
    
    private JLabel lblHeader;
    
    private JPanel addPane;
    private JCheckBox chkAccessors;
    private JCheckBox chkMutators;
    
    private JPanel optionsPane;
    private JComboBox cboCommentStyle;
    
    private JPanel runPane;
    //private JButton cmdHowToUse;
    private JButton cmdAddMethods;
    
    private JMenuBar menuBar;
    
    private JMenu [] menus;
    private JMenuItem quitItem;
    private JMenuItem helpItem;
    private JMenuItem aboutItem;
    
    private File lastSelectedFile;
    
    private SpringLayout layout;
    
    private MethodWriterHelp mwh;
    
    public MethodWriterGUI()
    {
        setupGUI();
    }//End of constructor
    
    private void setupGUI()
    {
        //Setup the frame
        this.getRootPane().setDefaultButton(cmdAddMethods);
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle(WINDOW_TITLE);
        this.setIconImage(new ImageIcon(this.getClass().getResource("images/icon.png")).getImage());
        
        this.setupMenu();
        
        //Get the header image
        this.lblHeader = new JLabel(new ImageIcon(this.getClass().getResource("images/header.png")));
        
        //Methods
        this.addPane = new JPanel(new GridLayout(1, 2, PADDING_SIZE, PADDING_SIZE));
        this.addPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(METHODS_SELECTION_PANEL_TITLE), BorderFactory.createEmptyBorder(PADDING_SIZE,PADDING_SIZE,PADDING_SIZE,PADDING_SIZE)));
        
        this.chkAccessors = new JCheckBox(ACCESSOR_METHODS_TITLE, true);
        this.chkAccessors.addActionListener(this);
        this.chkAccessors.setToolTipText("Add accessor or \"getter\" methods.");
        
        this.chkMutators = new JCheckBox(MUTATOR_METHODS_TITLE, true);
        this.chkMutators.addActionListener(this);
        this.chkMutators.setToolTipText("Add mutator or \"setter\" methods.");
        
        this.addPane.add(this.chkAccessors);
        this.addPane.add(this.chkMutators);
        
        this.addPane.setPreferredSize(new Dimension(FRAME_WIDTH - 3 * PADDING_SIZE, this.addPane.getPreferredSize().height));
        
        //Options
        this.optionsPane = new JPanel(new GridLayout(2, 2, PADDING_SIZE, PADDING_SIZE));
        this.optionsPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(OPTIONS_PANEL_TITLE), BorderFactory.createEmptyBorder(PADDING_SIZE,PADDING_SIZE,PADDING_SIZE,PADDING_SIZE)));
        
        this.optionsPane.add(new JLabel(COMMENTS_STYLE_TITLE + ":"));
        this.optionsPane.add(new JLabel(""));
        
        this.cboCommentStyle = new JComboBox();
        
        for (MethodCommentStyle mcs : MethodCommentStyle.values())
            this.cboCommentStyle.addItem(mcs.getName());
        
        this.cboCommentStyle.setSelectedItem("Simple");
        this.cboCommentStyle.addActionListener(this);
        
        this.optionsPane.add(this.cboCommentStyle);
        this.optionsPane.add(new JLabel(""));
        
        this.optionsPane.setPreferredSize(new Dimension(FRAME_WIDTH - 3 * PADDING_SIZE, this.optionsPane.getPreferredSize().height));
        
        //Buttons
        this.runPane = new JPanel(new GridLayout(1, 3, PADDING_SIZE, PADDING_SIZE));
        this.runPane.setBorder(BorderFactory.createEmptyBorder(PADDING_SIZE,PADDING_SIZE,PADDING_SIZE,PADDING_SIZE));

        //this.cmdHowToUse = new JButton(HOW_TO_USE_BUTTON_TITLE);
        this.cmdAddMethods = new JButton(ADD_METHODS_BUTTON_TITLE);
        this.cmdAddMethods.addActionListener(this);
        
        this.runPane.add(new JLabel(""));
        this.runPane.add(new JLabel(""));
        this.runPane.add(this.cmdAddMethods);
        
        this.runPane.setPreferredSize(new Dimension(FRAME_WIDTH - 3 * PADDING_SIZE, this.runPane.getPreferredSize().height));
        
        //Setup the layout
        this.layout = new SpringLayout();
        this.getContentPane().setLayout(this.layout);
        
        final int addPaneStart = 80;
        
        this.layout.putConstraint(SpringLayout.NORTH, this.lblHeader, 0, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.lblHeader, 0, SpringLayout.WEST, this.getContentPane());
        
        this.layout.putConstraint(SpringLayout.NORTH, this.addPane, addPaneStart, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.addPane, PADDING_SIZE, SpringLayout.WEST, this.getContentPane());
        
        this.layout.putConstraint(SpringLayout.NORTH, this.optionsPane, addPaneStart + this.addPane.getPreferredSize().height, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.optionsPane, PADDING_SIZE, SpringLayout.WEST, this.getContentPane());
        
        this.layout.putConstraint(SpringLayout.NORTH, this.runPane, addPaneStart + 10 + this.addPane.getPreferredSize().height + this.optionsPane.getPreferredSize().height, SpringLayout.NORTH, this.getContentPane());
        this.layout.putConstraint(SpringLayout.WEST, this.runPane, PADDING_SIZE, SpringLayout.WEST, this.getContentPane());
        
        //Add elements to the screen
        this.add(this.lblHeader);
        this.add(this.addPane);
        this.add(this.optionsPane);
        this.add(this.runPane);
        
        this.mwh = new MethodWriterHelp();
        this.mwh.setVisible(false);
 
        //Update components, if the resize was used
        SwingUtilities.updateComponentTreeUI(this);
        
        //Make the frame visible
        this.setVisible(true);
        
        //Adjust the sizing of the frame depending on the size of the menu bar
        //Varies per operating system
        int menuBarHeight = menuBar.getHeight();
        int bannerHeight = lblHeader.getHeight();
        int addPaneHeight = addPane.getHeight();
        int optionsPaneHeight = optionsPane.getHeight();
        int runPaneHeight = runPane.getHeight();
        int padding = 50;
        
        int height = menuBarHeight + bannerHeight + addPaneHeight + optionsPaneHeight + runPaneHeight + padding;
        
        this.setSize(FRAME_WIDTH, height);
    }//End of setupGUI method
    
    private void setupMenu()
    {
        final int fileMenu = 0;
        final int helpMenu = 1;
        
        //Menus
        this.menuBar = new JMenuBar();
        
        this.menus = new JMenu[]{new JMenu("File"), new JMenu("Help")};
        
        //FILE MENU
        this.quitItem = new JMenuItem("Quit");
        this.quitItem.addActionListener(this);
        this.quitItem.setAccelerator(KeyStroke.getKeyStroke("alt F4"));
        this.quitItem.setMnemonic('q');
        
        this.menus[fileMenu].setMnemonic('f');
        this.menus[fileMenu].add(this.quitItem);
        
        //Help MENU
        this.helpItem = new JMenuItem("Help");
        this.helpItem.addActionListener(this);
        this.helpItem.setAccelerator(KeyStroke.getKeyStroke("F1"));
        this.helpItem.setMnemonic('h');
        
        this.aboutItem = new JMenuItem("About");
        this.aboutItem.addActionListener(this);
        this.aboutItem.setMnemonic('a');
        
        this.menus[helpMenu].setMnemonic('h');
        this.menus[helpMenu].add(this.helpItem);
        this.menus[helpMenu].addSeparator();
        this.menus[helpMenu].add(this.aboutItem);
        
        for (JMenu menu : this.menus)
            this.menuBar.add(menu);
        
        //Add menu bar
        this.setJMenuBar(this.menuBar);
    }
    
    /**
     * Shows a file chooser and adds the checked methods to that file.
     *
     * @since 1.0
     */
    private void addMethods()
    {
        JFileChooser fc = new JFileChooser();
        
        if (lastSelectedFile != null)
            fc.setCurrentDirectory(lastSelectedFile.getParentFile());
        
        //Set the file choose to only allow .java files to be chosen
        fc.setFileFilter(new JavaFileFilter());
        fc.setAcceptAllFileFilterUsed(false);
        fc.setMultiSelectionEnabled(true);
        
        //Show the file chooser dialog
        int returnVal = fc.showDialog(this, "Add Methods");
        
        //Move on only if a file has been chosen
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            //Get the file that was chosen by the user
            final File [] files = fc.getSelectedFiles();
            
            for (final File file : files)
            {
                lastSelectedFile = file;
            
                JOptionPane workingDialog = new JOptionPane("Please wait while the methods are added to your file.\n\nThis dialog will close automatically when the operation is complete.", JOptionPane.INFORMATION_MESSAGE);
                final JDialog dialog = workingDialog.createDialog(this, "Working... | " + WINDOW_TITLE); //final to allow access in the thread
                
                final MethodWriterGUI that = this;
                
                //Run the method writing in a seperate thread to allow the dialog to be dismissed when the operation is complete
                new Thread(new Runnable()
                               {
                    public void run()
                    {
                        //Declare and initialize variables
                        Declaration [] declarations;
                        String accessorMethods = "";
                        String mutatorMethods = "";
                        
                        try
                        {
                            declarations = FieldFileParser.parseFile(file);
                            
                            //Add the accessor methods
                            if (chkAccessors.isSelected())
                            {
                                try
                                {
                                    accessorMethods = AccessorMethodWriter.getMethods(declarations);
                                }//End of try
                                /*catch (CannotAddMethodException e)
                                 {
                                 JOptionPane.showMessageDialog(that, "The accessor methods could not be added.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                                 }//End of catch (CannotAddMehthodsException)*/
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                                }//End of exception e
                            }//End of if
                            
                            //Add the mutator methods
                            if (chkMutators.isSelected())
                            {
                                try
                                {
                                    mutatorMethods = MutatorMethodWriter.getMethods(declarations);
                                }//End of try
                                /*catch (CannotAddMethodException e)
                                 {
                                 JOptionPane.showMessageDialog(that, "The mutator methods could not be added.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                                 }//End of catch (CannotAddMehthodsException)*/
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                                }//End of exception e
                            }//End of if
                            
                            WriteMethodsToFile.writeFile(file, accessorMethods, mutatorMethods);
                        }//End of try
                        catch (FileNotFoundException e)
                        {
                            JOptionPane.showMessageDialog(that, "The file you requested could not be found.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                        }//End of catch (FileNotFoundException)
                        catch (IOException e)
                        {
                            JOptionPane.showMessageDialog(that, "An error occured while trying to read the file.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                        }//End of catch (IOException)
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                        }//End of catch (Exception)
                        
                        //Close the wait dialog
                        dialog.dispose();
                    }//End of run method
                }).start(); //End of runnable and thead
                
                //Make the dialog visible
                dialog.setVisible(true);
            }
            
            //Show completed message
            JOptionPane.showMessageDialog(this, "All files have been updated with the selected methods.", WINDOW_TITLE, JOptionPane.INFORMATION_MESSAGE);
        }//End of if
    }//End of addMethods method
    
    /**
     * Method to show the "how to use" box.
     *
     * @since 1.0
     */
    private void showHowToUse()
    {
        JOptionPane.showMessageDialog(this, "In your Java source code, add the following comments:\n//accessors\n\twhere you want the accessor methods.\n//mutators\n\twhere you want the mutator methods.\n\nClick on 'Add Methods' and select the Java file you want to add the methods to.", WINDOW_TITLE, JOptionPane.PLAIN_MESSAGE);
    }//End of showHowToUse method
    
    /**
     * Method to disable the "Add Methods" button if none of the add options are selected.
     *
     * @since 1.0
     */
    private void verifyOneChecked()
    {
        if (chkAccessors.isSelected() || chkMutators.isSelected())
            cmdAddMethods.setEnabled(true);
        else
            cmdAddMethods.setEnabled(false);
    }//End of verifyOneChecked method
    
    /**
     * Change wheter the application adds comments or not
     * 
     * @since 2.0
     */
    private void changeWriteComments()
    {
        MethodCommentStyle mcs = MethodCommentStyle.NONE;
        
        for (MethodCommentStyle mc : MethodCommentStyle.values())
        {
            if (mc.getName() == (String)this.cboCommentStyle.getSelectedItem())
            {
                mcs = mc;
                break;
            }//End of if
        }//End of for
        
        AccessorMethodWriter.setMethodCommentStyle(mcs);
        MutatorMethodWriter.setMethodCommentStyle(mcs);
    }//End of changeWriteComments method
    
    /**
     * Method that is called whenever an action is made (by the user) on a GUI object.
     *
     * @param event The ActionEvent object of the event.
     *
     * @since 1.0
     */
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == cmdAddMethods)
            addMethods();
        //else if (e.getSource() == cmdHowToUse)
            //showHowToUse();
        else if (e.getSource() == chkAccessors || e.getSource() == chkMutators)
            verifyOneChecked();
        else if (e.getSource() == cboCommentStyle)
            changeWriteComments();
        else if (e.getSource() == quitItem)
            this.dispose();
        else if (e.getSource() == helpItem)
            this.mwh.setVisible(true);
        else if (e.getSource() == aboutItem)
            JOptionPane.showMessageDialog(this, "Copyright 2012 - Zachary Seguin\n\nVersion 1.3.1\n\nhttps://bitbucket.org/zachomedia/java-oop-accessor-and-mutator-method-writer/", WINDOW_TITLE, JOptionPane.PLAIN_MESSAGE, new ImageIcon(this.getClass().getResource("images/icon.png")));
    }//End of actionPerformed method
    
    public static void main(String [] args)
    {
        // set the look and feel to that of the current OS
        try
        {
            boolean nimbusfound = false;
            
            //Get system look
            final String SYSTEM_LOOK = UIManager.getSystemLookAndFeelClassName();
            
             //for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
             //{
                 //if ("Nimbus".equals(info.getName())) {
                 //    UIManager.setLookAndFeel(info.getClassName());
                 //    nimbusfound = true;
                 //    break;
                 //}
             //}
            
            //Set system look
             //if (!nimbusfound)
                 UIManager.setLookAndFeel(SYSTEM_LOOK);
        }//End of try
        catch (Exception e)
        {
            e.printStackTrace();
            //Do nothing, use default java look
        }//End of catch
        
        //Create an instance of itself to show the GUI
        new MethodWriterGUI();
    }//End of main method
}//End of class