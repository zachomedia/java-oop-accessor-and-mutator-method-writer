package seguin.OOPAccessorMutatorWriterGUI;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.UIManager.*;
import java.io.*;

import seguin.OOPAccessorMutatorWriter.*;

/**
 * Help screen that assist the user with the operation of the application.
 * 
 * @author Zachary Seguin
 * @version 1.1 23/05/2012
 * @since 1.2
 */ 
public class MethodWriterHelp extends JFrame
{
    /**
     * The width of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_WIDTH = 575;
    
    /**
     * The height of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_HEIGHT = 450;
    
    /**
     * The window title.
     *
     * @since 1.0
     */
    public static String WINDOW_TITLE = "Help - OOP Accessors and Mutators Writer";
    
    /*
     * JEditorPane holds the HTML document that is imported from HelpInstructions.html
     * 
     * @since 1.0
     */
    private JEditorPane document;
    
    private GridLayout layout;
    
    /**
     * Constructs the MethodWriterHelp GUI Interface.
     */
    public MethodWriterHelp()
    {
        setupGUI();
    }//End of constructor
    
    /**
     * Setups all of the GUI components.
     */
    private void setupGUI()
    {
        //Setup the frame
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle(WINDOW_TITLE);
        this.setIconImage(new ImageIcon(this.getClass().getResource("images/icon.png")).getImage());
        
        //Setup GUI components
        try
        {
            //this.document = new JEditorPane("text/html", "<h1><p>This is a test of the screen!</p>");
            this.document = new JEditorPane(this.getClass().getResource("HelpInstructions.html"));
        }//End of try
        catch (Exception e)
        {
            try
            {
                this.document = new JEditorPane("text/html", "<p>Sorry, an unknown error occured loading the help file.</p>");
            }//End of try
            catch (Exception ex)
            { 
                this.document = null;
            }//End of catch
        }//End of catch
        
        this.document.setEditable(false);
        
        //Setup the layout
        this.layout = new GridLayout();
        this.getContentPane().setLayout(this.layout);
        
        this.add(new JScrollPane(this.document, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER));
 
        //Update components, if the resize was used
        SwingUtilities.updateComponentTreeUI(this);
    }//End of setupGUI method
}//End of class