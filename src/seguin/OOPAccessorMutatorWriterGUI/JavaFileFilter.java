package seguin.OOPAccessorMutatorWriterGUI;

//Import the required packages
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

/**
 * The JavaFileFilter for only allowing .java files to be accepted by the program.
 *
 * @author Zachary Seguin
 *
 * @version 1.1 15/02/2012
 * @since 1.0
 *
 */
public class JavaFileFilter extends FileFilter
{
    /**
     * Wheter the file is accepted and should be shown in the file chooser.
     *
     * @param file The file to check
     *
     * @return Wheter the file is of acceptable file type.
     *
     * @since 1.0
     */
    public boolean accept(File file)
    {
        try
        {
            if (file.isDirectory())
                return true;
            
            //Find the last period (in order to get the extends)
            int lastPeriod = file.getName().lastIndexOf(".");
            
            //Get the extension of the file
            String extension = file.getName().substring(lastPeriod);
            
            //Return true if the extension is .java
            if (extension.equals(".java"))
                return true;
            else
                return false;
        }//End of try
        catch (Exception e)
        {
            return false;
        }//End of catch
    }//End of accept method
    
    /**
     * Returns the description of this file filter.
     *
     * @return The description of the file filter.
     *
     * @since 1.0
     */
    public String getDescription()
    {
        return "Java Source File (.java)";
    }//End of getDescription method
}//End of class