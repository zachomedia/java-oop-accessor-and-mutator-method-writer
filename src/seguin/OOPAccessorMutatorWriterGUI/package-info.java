/**
 * Provides the GUI interface for automatically adding Accessor and Mutator methods to a Java class.
 * 
 * @since 2.0
 */
package seguin.OOPAccessorMutatorWriterGUI;