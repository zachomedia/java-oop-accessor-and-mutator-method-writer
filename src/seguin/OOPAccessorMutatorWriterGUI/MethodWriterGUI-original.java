package seguin.OOPAccessorMutatorWriterGUI;

//Import the required packages.
import seguin.OOPAccessorMutatorWriter.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

/**
 * The GUI for adding Accessors and Mutators to a class.
 *
 * @author Zachary Seguin
 *
 * @version 2.0 08/02/2012
 * @since 1.0
 *
 */
public class MethodWriterGUI extends JFrame implements ActionListener
{
    /**
     * The width of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_WIDTH = 375;
    
    /**
     * The height of the JFrame.
     *
     * @since 1.0
     */
    public static int FRAME_HEIGHT = 275;
    
    /**
     * The width of the padding around the JFrame.
     *
     * @since 1.0
     */
    public static int PADDING_SIZE = 5;
    
    /**
     * The window title.
     *
     * @since 1.0
     */
    public static String WINDOW_TITLE = "OOP Accessors and Mutators Writer | Designed by Zachary Seguin";
    
    private JPanel titlePane = new JPanel();
    private JLabel lblTitle = new JLabel("OOP Accessors and Mutators Writer");
    private JLabel lblSubtitle = new JLabel("Designed by Zachary Seguin");
    
    private JPanel addPane = new JPanel();
    private JCheckBox chkAccessors = new JCheckBox("Accessor Methods", true);
    private JCheckBox chkMutators = new JCheckBox("Mutator Methods", true);
    
    private JPanel optionsPane = new JPanel();
    private JCheckBox chkComments = new JCheckBox("Write Comments", true);
    
    private JPanel runPane = new JPanel();
    private JButton cmdHowToUse = new JButton("How To Use");
    private JButton cmdAddMethods = new JButton("Add Methods");
    
    private File lastSelectedFile;
    
    /**
     * The constructor to create the GUI.
     *
     * @since 1.0
     */
    public MethodWriterGUI()
    {
        //Add the border to the content pane
        JPanel contentPane = new JPanel();
        contentPane.setBorder(BorderFactory.createEmptyBorder(PADDING_SIZE, PADDING_SIZE, PADDING_SIZE, PADDING_SIZE));
        this.setContentPane(contentPane);
        
        //Setup the GUI Components
        titlePane.setLayout(new BorderLayout());
        lblTitle.setFont(lblTitle.getFont().deriveFont(Font.BOLD).deriveFont(20.0F));
        titlePane.add(lblTitle, BorderLayout.CENTER);
        lblSubtitle.setFont(lblTitle.getFont().deriveFont(Font.ITALIC).deriveFont(10.0F));
        titlePane.add(lblSubtitle, BorderLayout.SOUTH);
        
        addPane.setLayout(new GridLayout(1, 2));
        addPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Add"), BorderFactory.createEmptyBorder(5,5,5,5)));
        chkAccessors.addActionListener(this);
        chkAccessors.setToolTipText("Add accessor or \"getter\" methods.");
        addPane.add(chkAccessors);
        chkMutators.addActionListener(this);
        chkMutators.setToolTipText("Add mutator or \"setter\" methods.");
        addPane.add(chkMutators);
        
        optionsPane.setLayout(new GridLayout(1, 2));
        optionsPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Settings"), BorderFactory.createEmptyBorder(5,5,5,5)));
        chkComments.addActionListener(this);
        chkComments.setToolTipText("Write Javadoc comments above the accessor/mutator methods.");
        optionsPane.add(chkComments);
        
        runPane.setLayout(new GridLayout(1, 3));
        runPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        cmdHowToUse.addActionListener(this);
        runPane.add(cmdHowToUse);
        runPane.add(new JLabel());
        cmdAddMethods.addActionListener(this);
        runPane.add(cmdAddMethods);
        
        //Setup the layout manager
        this.getContentPane().setLayout(new BorderLayout(15, 15));//new GridLayout(3, 1, 15, 15));
        
        JPanel centerPane = new JPanel(new GridLayout(2, 1, 15, 15));
        centerPane.add(addPane);
        centerPane.add(optionsPane);
        
        //Add GUI Components to the screen
        this.add(titlePane, BorderLayout.NORTH);
        this.add(centerPane, BorderLayout.CENTER);
        this.add(runPane, BorderLayout.SOUTH);
        
        //Setup the frame
        this.getRootPane().setDefaultButton(cmdAddMethods);
        this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle(WINDOW_TITLE);
        
        this.setVisible(true);
        
        //Ensure comments are up-to date
        changeWriteComments();
        
        cmdAddMethods.requestFocus();
    }//End of constructor method
    
    /**
     * Shows a file chooser and adds the checked methods to that file.
     *
     * @since 1.0
     */
    private void addMethods()
    {
        JFileChooser fc = new JFileChooser();
        
        if (lastSelectedFile != null)
            fc.setCurrentDirectory(lastSelectedFile.getParentFile());
        
        //Set the file choose to only allow .java files to be chosen
        fc.setFileFilter(new JavaFileFilter());
        fc.setAcceptAllFileFilterUsed(false);
        
        //Show the file chooser dialog
        int returnVal = fc.showDialog(this, "Add Methods");
        
        //Move on only if a file has been chosen
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            //Get the file that was chosen by the user
            final File file = fc.getSelectedFile();
            lastSelectedFile = file;
            
            JOptionPane workingDialog = new JOptionPane("Please wait while the methods are added to your file.\n\nThis dialog will close automatically when the operation is complete.", JOptionPane.INFORMATION_MESSAGE);
            final JDialog dialog = workingDialog.createDialog(this, "Working... | " + WINDOW_TITLE); //final to allow access in the thread
            
            final MethodWriterGUI that = this;
            
            //Run the method writing in a seperate thread to allow the dialog to be dismissed when the operation is complete
            new Thread(new Runnable()
                           {
                public void run()
                {
                    //Declare and initialize variables
                    Declaration [] declarations;
                    String accessorMethods = "";
                    String mutatorMethods = "";
                    
                    try
                    {
                        declarations = FieldFileParser.parseFile(file);
                        
                        //Add the accessor methods
                        if (chkAccessors.isSelected())
                        {
                            try
                            {
                                accessorMethods = AccessorMethodWriter.getMethods(declarations);
                            }//End of try
                            /*catch (CannotAddMethodException e)
                             {
                             JOptionPane.showMessageDialog(that, "The accessor methods could not be added.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                             }//End of catch (CannotAddMehthodsException)*/
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                            }//End of exception e
                        }//End of if
                        
                        //Add the mutator methods
                        if (chkMutators.isSelected())
                        {
                            try
                            {
                                mutatorMethods = MutatorMethodWriter.getMethods(declarations);
                            }//End of try
                            /*catch (CannotAddMethodException e)
                             {
                             JOptionPane.showMessageDialog(that, "The mutator methods could not be added.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                             }//End of catch (CannotAddMehthodsException)*/
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                            }//End of exception e
                        }//End of if
                        
                        WriteMethodsToFile.writeFile(file, accessorMethods, mutatorMethods);
                    }//End of try
                    catch (FileNotFoundException e)
                    {
                        JOptionPane.showMessageDialog(that, "The file you requested could not be found.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                    }//End of catch (FileNotFoundException)
                    catch (IOException e)
                    {
                        JOptionPane.showMessageDialog(that, "An error occured while trying to read the file.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                    }//End of catch (IOException)
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(that, "An unkown error occured. Please restart the application and try again.", "ERROR - " + WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
                    }//End of catch (Exception)
                    
                    //Close the wait dialog
                    dialog.dispose();
                }//End of run method
            }).start(); //End of runnable and thead
            
            //Make the dialog visible
            dialog.setVisible(true);
            
            //Show completed message
            JOptionPane.showMessageDialog(this, file.getName() + " has been updated with the selected methods.", WINDOW_TITLE, JOptionPane.INFORMATION_MESSAGE);
        }//End of if
    }//End of addMethods method
    
    /**
     * Method to show the "how to use" box.
     *
     * @since 1.0
     */
    private void showHowToUse()
    {
        JOptionPane.showMessageDialog(this, "In your Java source code, add the following comments:\n//accessors\n\twhere you want the accessor methods.\n//mutators\n\twhere you want the mutator methods.\n\nClick on 'Add Methods' and select the Java file you want to add the methods to.", WINDOW_TITLE, JOptionPane.PLAIN_MESSAGE);
    }//End of showHowToUse method
    
    /**
     * Method to disable the "Add Methods" button if none of the add options are selected.
     *
     * @since 1.0
     */
    private void verifyOneChecked()
    {
        if (chkAccessors.isSelected() || chkMutators.isSelected())
            cmdAddMethods.setEnabled(true);
        else
            cmdAddMethods.setEnabled(false);
    }//End of verifyOneChecked method
    
    /**
     * Change wheter the application adds comments or not
     * 
     * @since 2.0
     */
    private void changeWriteComments()
    {
        AccessorMethodWriter.writeMethodComments(chkComments.isSelected());
        MutatorMethodWriter.writeMethodComments(chkComments.isSelected());
    }//End of changeWriteComments method
    
    /**
     * Method that is called whenever an action is made (by the user) on a GUI object.
     *
     * @param event The ActionEvent object of the event.
     *
     * @since 1.0
     */
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == cmdAddMethods)
            addMethods();
        else if (e.getSource() == cmdHowToUse)
            showHowToUse();
        else if (e.getSource() == chkAccessors || e.getSource() == chkMutators)
            verifyOneChecked();
        else if (e.getSource() == chkComments)
            changeWriteComments();
    }//End of actionPerformed method
    
    /**
     * The method to start the application. Shows the GUI.
     *
     * @param args The arguments from the Command Line. <i>Not used in this application.</i>
     *
     * @since 1.0
     */
    public static void main (String [] args)
    {
        // set the look and feel to that of the current OS
        try
        {
            //Get system look
            final String SYSTEM_LOOK = UIManager.getSystemLookAndFeelClassName();
            
            //Set system look
            UIManager.setLookAndFeel(SYSTEM_LOOK);
        }//End of try
        catch (Exception e)
        {
            e.printStackTrace();
            //Do nothing, use default java look
        }//End of catch
        
        //Create an instance of itself to show the GUI
        new MethodWriterGUI();
    }//End of main method
}//End of class