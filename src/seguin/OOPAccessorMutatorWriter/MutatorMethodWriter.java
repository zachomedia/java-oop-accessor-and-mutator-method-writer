package seguin.OOPAccessorMutatorWriter;

/**
 * Write the mutator metods for each of the provided declarations.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.1, 09/03/2012
 */
public class MutatorMethodWriter extends JavaMethodWriter
{    
    /**
     * Get the method comment, header, body and footer for the provided declaration.
     * 
     * @param declaration The declaration object to write the method for.
     * 
     * @return The method.
     * 
     * @since 1.0
     */
    public static String getMethod(Declaration declaration)
    {
        //Declare and initialize variables
        String method = "";
        String methodName = "";
        String array = "";
        
        for (int x = 0; x < declaration.getArrayLevel(); x++)
            array += "[]";
        
        //Write the method comment
        switch (getMethodCommentStyle())
        {
            case SIMPLE:
                method += getFormattedString("//Sets the value of " + declaration.getVariableName() + ".");
                break;
            
            case JAVADOC:
                method += getFormattedString("/**");
                method += getFormattedString(" * Sets the value of " + declaration.getVariableName() + ".");
                method += getFormattedString(" *");
                if (declaration.isArray())
                    method += getFormattedString(" * @param " + declaration.getVariableName() + " " + array + "  The new value for " + declaration.getVariableName() + ".");
                else
                    method += getFormattedString(" * @param " + declaration.getVariableName() + " The new value for " + declaration.getVariableName() + ".");
                
                method += getFormattedString(" */");
                break;
        }//End of swtich
        
        //Create the method name
        if (declaration.getDataType().equals("boolean"))
        {
            if (declaration.getVariableName().startsWith("set") && declaration.getVariableName().length() > 2)
            {
                methodName = "set" + declaration.getVariableName().substring(2, 3).toUpperCase().charAt(0) + declaration.getVariableName().substring(3);
            }//End of if
            else
                methodName = "set" + declaration.getVariableName().substring(0, 1).toUpperCase().charAt(0) + declaration.getVariableName().substring(1);
        }//End of if
        else
            methodName = "set" + declaration.getVariableName().substring(0, 1).toUpperCase().charAt(0) + declaration.getVariableName().substring(1);
        
        //Write the method
        if (declaration.isArray())
            method += getFormattedString("public void " + methodName + "(" + declaration.getDataType() + " " + array + " " + declaration.getVariableName() + ")");
        else
            method += getFormattedString("public void " + methodName + "(" + declaration.getDataType() + " " + declaration.getVariableName() + ")");
            
        method += getFormattedString("{");
        method += getFormattedString("this." + declaration.getVariableName() + " = " + declaration.getVariableName() + ";", 2);
        
        if (getMethodCommentStyle() != MethodCommentStyle.NONE)
            method += getFormattedString("}//End of " + methodName + " method");
        else
            method += getFormattedString("}");
        
        //Add a blank line
        method += getFormattedString("");
        
        //Return the method
        return method;
    }//End of getMethod method
    
    /**
     * Write the accessor method for each of the provided declarations.
     * 
     * @param declarations The list of declarations to write methods for.
     * 
     * @return The written methods.
     * 
     * @since 1.0
     */
    public static String getMethods(Declaration [] declarations)
    {
        //Return an empty string if there are no declarations
        if (declarations.length == 0)
            return "";
        
        //Declare and initialize variables
        String methods = "";
        
        //Add blank line
        methods += getFormattedString("");
        
        //Write the "section header" comment
        if (getMethodCommentStyle() != MethodCommentStyle.NONE)
        {
            methods += getFormattedString("/*******************");
            methods += getFormattedString(" * MUTATOR METHODS *");
            methods += getFormattedString(" *******************/");
            methods += getFormattedString("");
        }//End of if
        
        //Write the methods for each declaration
        for (Declaration declaration : declarations)
            methods += getMethod(declaration);
        
        //Add the footer to the section
        if (getMethodCommentStyle() != MethodCommentStyle.NONE)
        {
            methods += getFormattedString("/**************************");
            methods += getFormattedString(" * END OF MUTATOR METHODS *");
            methods += getFormattedString(" **************************/");
        }//End of if
        
        //Return the methods
        return methods;
    }//End of addMethods method
}//End of class