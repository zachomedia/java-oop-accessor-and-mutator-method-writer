package seguin.OOPAccessorMutatorWriter;

import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Writing methods to file class.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.0, 29/02/2012
 */
public class WriteMethodsToFile
{
    /**
     * The text to replace with the accessors method.
     * 
     * @since 1.0
     */
    private static String accessorReplaceText = "//accessors";
    
    /**
     * The text to replace with the mutator method.
     * 
     * @since 1.0
     */
    private static String mutatorReplaceText = "//mutators";
    
    /**
     * Get the text that is to be replaced with the accessor methods
     * 
     * @return The accessor replace text.
     * 
     * @since 1.0
     */
    public static String getAccessorReplaceText()
    {
        return accessorReplaceText;
    }//End of getAccessorReplaceText method
    
    /**
     * Get the text that is to be replaced with the mutator methods
     * 
     * @return The mutator replace text.
     * 
     * @since 1.0
     */
    public static String getMutatorReplaceText()
    {
        return mutatorReplaceText;
    }//End of getMutatorReplaceText method
    
    /**
     * Set the text that is to be replaced with the accessor methods
     * 
     * @param accessorReplaceText The new accessor replace text.
     * 
     * @since 1.0
     */
    public static void getAccessorReplaceText(String newAccessorReplaceText)
    {
        accessorReplaceText = newAccessorReplaceText;
    }//End of setAccessorReplaceText method
    
    /**
     * Set the text that is to be replaced with the mutator methods
     * 
     * @param mutatorReplaceText The new mutator replace text.
     * 
     * @since 1.0
     */
    public static void setMutatorReplaceText(String newMutatorReplaceText)
    {
        mutatorReplaceText = newMutatorReplaceText;
    }//End of setMutatorReplaceText method
    
    /**
     * Write the accessor and mutator methods to file
     * 
     * @param file The file to put methods in.
     * @param accessorMethods The accessor methods to add to the file.
     * @param mutatorMethods The mutator methods to add to the file.
     * 
     * @since 1.0
     */
    public static void writeFile(File file, String accessorMethods, String mutatorMethods) throws FileNotFoundException, IOException
    {   
        String fileContents = FieldFileParser.getFileContents(file);

        fileContents = fileContents.replace(accessorReplaceText, accessorMethods);
        fileContents = fileContents.replace(mutatorReplaceText, mutatorMethods);
        
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file)), false);
        
        writer.print(fileContents);
        
        writer.close();
    }//End of writeFile method
}//End of class