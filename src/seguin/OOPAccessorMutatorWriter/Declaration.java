package seguin.OOPAccessorMutatorWriter;

/**
 * Stores information that is important to Java declerations.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.3, 26/05/2012
 */
public class Declaration
{
    /**
     * Stores the variable name for the declaration.
     * 
     * @since 1.0
     */
    private String variableName;
    
    /**
     * Stores the data type of the declaration.
     * 
     * @since 1.0
     */
    private String dataType;
    
    /**
     * Is the Declaration an array?
     * 
     * @since 1.2
     * @deprecated 1.3 Please use the isArray() method and the arrayLevel field instead.
     */
    private boolean isArray;
    
     /**
     * Stores the level of the array. 0 means it is not an array.
     * 
     * @since 1.3
     */
    private int arrayLevel;
    
    /**
     * Creates an instance of the declaration object with a default dataType of type Object.
     * 
     * @param variableName The name of the variable.
     * 
     * @since 1.0
     */
    public Declaration(String variableName)
    {
        this.variableName = variableName;
        this.dataType = "Object";
        this.isArray = false;
        this.arrayLevel = 0;
    }//End of constructor method
    
    /**
     * Creates an instance of the declaration object.
     * 
     * @param variableName The name of the variable.
     * @param dataType The data type of the object.
     * 
     * @since 1.0
     */
    public Declaration(String variableName, String dataType)
    {
        this.variableName = variableName;
        this.dataType = dataType;
        this.isArray = false;
        this.arrayLevel = 0;
    }//End of constructor
    
    /**
     * Creates an instance of the declaration object.
     * 
     * @param variableName The name of the variable.
     * @param dataType The data type of the object.
     * @param isArray Is the declaration an array.
     * 
     * @since 1.0
     * @deprecated 1.3 Please use Declaration(String variableName, String dataType, int arrayLevel) instead.
     */
    public Declaration(String variableName, String dataType, boolean isArray)
    {
        this.variableName = variableName;
        this.dataType = dataType;
        this.isArray = isArray;
        this.arrayLevel = 1;
    }//End of constructor
    
    /**
     * Creates an instance of the declaration object.
     * 
     * @param variableName The name of the variable.
     * @param dataType The data type of the object.
     * @param arrayLevel The level of the array. 0 means not array.
     * 
     * @since 1.0
     */
    public Declaration(String variableName, String dataType, int arrayLevel)
    {
        this.variableName = variableName;
        this.dataType = dataType;
        
        if (arrayLevel > 0)
            this.isArray = isArray;
        
        this.arrayLevel = arrayLevel;
    }//End of constructor
    
    /**
     * Get the variable name of the declaration.
     * 
     * @return The variable name.
     * 
     * @since 1.0
     */
    public String getVariableName()
    {
        return this.variableName;
    }//End of getVariableName method
    
    /**
     * Get the data type of the declaration.
     * 
     * @return The data type.
     * 
     * @since 1.0
     */
    public String getDataType()
    {
        return this.dataType;
    }//End of getDataType method
    
    /**
     * Get if the declaration is an array or not.
     * 
     * @return Is the declaration an array.
     * 
     * @since 1.2
     */
    public boolean isArray()
    {
        return this.arrayLevel > 0;
    }//End of isArray method
    
    /**
     * Get the level of the array
     * 
     * @return Is the declaration an array.
     * 
     * @since 1.2
     */
    public int getArrayLevel()
    {
        return this.arrayLevel;
    }//End of isArray method
    
    /**
     * Set the variable name of the declaration.
     * 
     * @param newVariableName The new variable name for the declaration.
     * 
     * @since 1.0
     */
    public void setVariableName(String newVariableName)
    {
        this.variableName = newVariableName;
    }//End of setVariableName method
    
    /**
     * Set the data type of the declaration.
     * 
     * @param newDataType The new data type for the declaration.
     * 
     * @since 1.0
     */
    public void setDataType(String newDataType)
    {
        this.dataType = newDataType;
    }//End of setDataType method
    
     /**
     * Set wheter the declaration is an array or not.
     * 
     * @param isArray Is the declaration an array.
     * 
     * @since 1.2
     * @deprecated 1.3 Please use setArrayLevel instead.
     */
    public void setIsArray(boolean isArray)
    {
        this.isArray = isArray;
    }//End of setIsArray method
    
    /**
     * Set the array level.
     * 
     * @param arrayLevel The level of the array. 0 means not array.
     * 
     * @since 1.3
     */
    public void setArrayLevel(int arrayLevel)
    {
        if (arrayLevel > 0)
           this.isArray = true;
        
        this.arrayLevel = arrayLevel;
    }//End of setIsArray method
}//End of class