package seguin.OOPAccessorMutatorWriter;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Parse a Java file and indentify the fields present.
 * <br /><br />
 * <i>Fixed a bug where the fields that are initialized are not registered. Also, the application now pickups both private and protected fields.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.1, 09/03/2012
 */
public class FieldFileParser
{
    /**
     * The standard expression for finding the classes's fields.
     *
     * @since 1.0
     */
    public final static String FIELD_REGEX = "((?:private|protected) [a-zA-z<>]+ [a-zA-z\\d]+.*;.*\\n)";
    
    /**
     * The standard expression for seperating the data type and the variable name.
     *
     * @since 1.0
     */
    public final static String DECLARATION_REGEX = "(?:private|protected) ([a-zA-z<>]+)(?:[\\s\\[\\]]+)([a-zA-z$_\\d]+).*;.*\\n";
    
    public static String getFileContents(File file) throws FileNotFoundException, IOException
    {
        //Declare and initialize variables
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String input = "";
        String fileContents = "";
        
        //Loop until there is no more content in the file.
        while(input != null)
        {
            try
            {
                //Get the line from the file.
                input = reader.readLine();
                
                //Add a blank line if there the program has already imported other lines from the file.
                if (fileContents.length() > 0 && input != null)
                    fileContents += "\n";
                
                //Only add to the contents variable if there is acutally input waiting.
                if (input != null)
                    fileContents += input;
            }//End of try
            catch (Exception e)
            {
                //do nothing...
            }//End of catch
        }//End of while loop
        
        //Close the file
        reader.close();
        
        return fileContents;
    }//End of getFileContents method
    
    
    /**
     * Get the declarations (fields) from the provided Java file.
     * 
     * @param file The file to parse.
     * 
     * @since 1.0
     */
    public static Declaration [] parseFile(File file) throws FileNotFoundException, IOException
    {
        //Declare and initialize variables
        String fileContents = "";
        String dataType = "";
        String variableName = "";
        Vector<String> matches = new Vector<String>();
        Declaration [] declarations;
        Pattern pattern;
        Matcher matcher;
        boolean isArray;
        int arrayLevel;
        
        //Load the contents of the file
        fileContents = getFileContents(file);
        
        //Compile the regular expression (for fields) and compare it against the provided text.
        pattern = Pattern.compile(FIELD_REGEX);
        matcher = pattern.matcher(fileContents);
        
        boolean matchFound = matcher.find();
        
        //Run the loop while there is still a match found.
        while (matchFound)
        {
            if (!matcher.group().contains("static"))
                matches.add(matcher.group(0));
            
            //Get the next match starting at the last character from the previous match.
            matchFound = matcher.find(matcher.end());
        }//End of while loop
        
        declarations = new Declaration[matches.size()];
        
        //Get individual declarations
        for (int x = 0; x < matches.size(); x++)
        {
            pattern = Pattern.compile(DECLARATION_REGEX);
            matcher = pattern.matcher(matches.get(x));
            
            matchFound = matcher.find();
            isArray = matcher.group().contains("[]");
            arrayLevel = 0;
            
            //Count the array dimension
            String match = matcher.group();
            
            while (isArray && match.contains("[]"))
            {
               match = match.replaceFirst("\\[\\]", "");
               arrayLevel ++;
            }//End of while
            
            System.out.println();
            
            if (matchFound && matcher.groupCount() == 2)
            {
                declarations[x] = new Declaration(matcher.group(2), matcher.group(1), arrayLevel);
            }//End of if
        }//End of for
        
        return declarations;
    }//End of parseFile method
}//End of class