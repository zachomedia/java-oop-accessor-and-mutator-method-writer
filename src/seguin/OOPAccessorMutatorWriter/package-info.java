/**
 * Provides the necessary classes for automatic creation of Accessor and Mutator methods for Java classes.
 * 
 * @since 1.0
 */
package seguin.OOPAccessorMutatorWriter;