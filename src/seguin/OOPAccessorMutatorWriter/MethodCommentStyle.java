package seguin.OOPAccessorMutatorWriter;

/**
 * Write the accessor metods for each of the provided declarations.
 *
 * @author Zachary Seguin
 * @since 1.2
 * @version 1.0, 18/04/2012
 */
public enum MethodCommentStyle
{
    /**
     * No comment are written. See {@link https://bitbucket.org/zachomedia/java-oop-accessor-and-mutator-method-writer/wiki/Method%20Comments%20Style}
     */
    NONE("None"),
        
    /**
     * Simple style. See {@link https://bitbucket.org/zachomedia/java-oop-accessor-and-mutator-method-writer/wiki/Method%20Comments%20Style}
     */
    SIMPLE("Simple"),
    
    /**
     * Javadoc style. See {@link https://bitbucket.org/zachomedia/java-oop-accessor-and-mutator-method-writer/wiki/Method%20Comments%20Style}
     */
    JAVADOC("Javadoc");
    
    private final String name;
    
    MethodCommentStyle(String name)
    {
        this.name = name;
    }
    
    /**
     * Get the name of the style.
     * 
     * @return The name of the style.
     */
    public String getName()
    {
        return this.name;
    }//End of getName method
}//End of enumeration