package seguin.OOPAccessorMutatorWriter;

/**
 * For one reason or another, a method could not be added to a document.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.0, 29/02/2012
 */
public class CannotAddMethodException extends Exception
{
    /**
     * Creates an exception.
     */
    public CannotAddMethodException()
    {
        super();
    }//End of constructor method
    
    /**
     * Creates an exception with the provided message.
     */
    public CannotAddMethodException(String message)
    {
        super(message);
    }//End of constructor method
}//End of class