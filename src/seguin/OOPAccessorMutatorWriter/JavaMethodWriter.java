package seguin.OOPAccessorMutatorWriter;

/**
 * A "template" that includes methods favourable to all Java method writers.
 * 
 * @author Zachary Seguin
 * @since 1.0
 * @version 1.1, 18/04/2012
 */
public abstract class JavaMethodWriter
{
    /**
     * Wheter the method writer should include method comments.
     *
     * @since 1.0
     * @deprecated 1.2 Deprecated in 1.2, use methodCommentStyle instead. This boolean has no effect on the comments.
     */
    @Deprecated
    private static boolean writeMethodComments = true;
    
    /**
     * The comment style that should be written.
     *
     * @since 1.2
     */
    private static MethodCommentStyle methodCommentStyle = MethodCommentStyle.SIMPLE;
    
    /**
     * Sets if the method writer includes comments.
     *
     * @param writeComments Wheter the method writer shall include comments.
     *
     * @since 1.0
     * @deprecated 1.2, Use {@link #setMethodCommentStyle(MethodCommentStyle)} instead
     */
    @Deprecated
    public static void writeMethodComments(boolean writeComments)
    {
        writeMethodComments = writeComments;
    }//End of writeMethodComments method
    
    /**
     * Gets if the method writer includes comments.
     *
     * @return If the method writer is writing comments.
     *
     * @since 1.0
     * @deprecated 1.2, Use {@link #getMethodCommentStyle()} instead
     */
    @Deprecated
    public static boolean isWritingComments()
    {
        return writeMethodComments;
    }//End of writeMethodComments method
    
    /**
     * Sets the style of method comments to be written.
     *
     * @param methodCommentStyle The method style to write.
     *
     * @since 1.1
     */
    public static void setMethodCommentStyle(MethodCommentStyle newMethodCommentStyle)
    {
        methodCommentStyle = newMethodCommentStyle;
    }//End of setMethodCommentStyle method
    
    /**
     * Gets the style of method comments to be written.
     *
     * @return The method style to write.
     *
     * @since 1.1
     */
    public static MethodCommentStyle getMethodCommentStyle()
    {
        return methodCommentStyle;
    }//End of setMethodCommentStyle method
    
    /**
     * Returns the provided String with the one (1) tab pre-pended.
     * 
     * @param text The text to format.
     * 
     * @return The formatted String.
     * 
     * @since 1.0
     */
    public static String getFormattedString(String text)
    {
        return getFormattedString(text, 1);
    }//End of getFormattedString method
    
    /**
     * Returns the provided String with the number of tabs (indent level).
     * 
     * @param text The text to format.
     * @param indentLevel The indent level (or number of tabs).
     * 
     * @return The formatted String.
     * 
     * @since 1.0
     */
    public static String getFormattedString(String text, int indentLevel)
    {
        //Declare and initialize variables
        String formatted = "";
        
        //Add the indents (tabs)
        for (int x = 0; x < indentLevel; x++)
            formatted += "\t";
        
        //Add the text
        formatted += text;
        
        //Move the cursor to the next line
        formatted += "\n";
        
        //Return the formatted string.
        return formatted;
    }//End of getFormattedString method
    
}//End of class